// ----------------------------------------------------------------------
// File: Defaults.hh
// Author: Georgios Bitzes - CERN
// ----------------------------------------------------------------------

/****************************************************************************
 * Copyright (C) 2016 CERN/Switzerland                                      *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 *     http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ***************************************************************************/

#ifndef __GRID_HAMMER_DEFAULTS_H__
#define __GRID_HAMMER_DEFAULTS_H__

#define DEFAULT_FIRSTFILE 0
#define DEFAULT_LASTFILE 10000
#define DEFAULT_MAXINFLIGHT 1000
#define DEFAULT_NTHREADS 8
#define DEFAULT_INITIALIZATION false



#endif
