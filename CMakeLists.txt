cmake_minimum_required(VERSION 2.6)
project(grid-hammer)
include(GNUInstallDirs)
option(PACKAGEONLY "Only generate source RPMs" OFF)

#-------------------------------------------------------------------------------
# Search for dependencies
#-------------------------------------------------------------------------------

set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)
include(CXX11Support)

if(NOT PACKAGEONLY)
  find_package(Threads REQUIRED)
  find_package(davix REQUIRED)
  find_package(XRootD REQUIRED)
  find_package(GFAL2 REQUIRED)
endif()

#-------------------------------------------------------------------------------
# Regenerate Version.hh
#-------------------------------------------------------------------------------

add_custom_target(GenerateVersionInfo ALL DEPENDS Version)
add_custom_command(
  OUTPUT Version
  COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/genversion.py
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)

#-------------------------------------------------------------------------------
# Compiler options
#-------------------------------------------------------------------------------

add_definitions(-Wall -Wextra -Werror -Wno-unused-parameter -g3 ${CXX11_FLAG_ENABLE})

#-------------------------------------------------------------------------------
# Build all test programs
#-------------------------------------------------------------------------------

if(NOT PACKAGEONLY)
  add_subdirectory(src)
  add_subdirectory(deps/backward-cpp)
endif()

#-------------------------------------------------------------------------------
# Extract values from Version.hh for cpack
#-------------------------------------------------------------------------------

include(ExtractVersionInfo)
ExtractVersionInfo()

#-------------------------------------------------------------------------------
# Install the python scripts
#-------------------------------------------------------------------------------

install(
  PROGRAMS hammer-runner.py hammer-visualizer.py
  DESTINATION ${CMAKE_INSTALL_FULL_BINDIR}
)

#-------------------------------------------------------------------------------
# Packaging and source rpm
#-------------------------------------------------------------------------------

set(CPACK_SOURCE_GENERATOR "TGZ")
set(CPACK_PACKAGE_NAME "${CMAKE_PROJECT_NAME}")
set(CPACK_PACKAGE_VERSION "${GRIDHAMMER_VERSION}")
set(CPACK_SOURCE_PACKAGE_FILE_NAME "${CMAKE_PROJECT_NAME}-${CPACK_PACKAGE_VERSION}")
set(GRIDHAMMER_ARCHIVE "${CPACK_SOURCE_PACKAGE_FILE_NAME}.tar.gz")

set(CPACK_SOURCE_IGNORE_FILES
"/build/;/.git/;${CPACK_SOURCE_IGNORE_FILES};")

include(CPack)
add_custom_target(
  configure_specfile
  COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/genversion.py --template packaging/grid-hammer.spec.in --out packaging/grid-hammer.spec
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)

add_custom_target(
  dist
  COMMAND ${CMAKE_MAKE_PROGRAM} package_source
  DEPENDS configure_specfile
)

set (RPM_DEFINE --define "_source_filedigest_algorithm md5" --define "_binary_filedigest_algorithm md5")
add_custom_target(
  srpm
  COMMAND rpmbuild ${RPM_DEFINE} -ts ${GRIDHAMMER_ARCHIVE} --define "_topdir ${CMAKE_BINARY_DIR}" --with server
  DEPENDS dist
)
