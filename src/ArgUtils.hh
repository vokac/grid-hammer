// --------------------------------------------------------------------------
// File: ArgUtils.hh
// Author: Georgios Bitzes - CERN
// --------------------------------------------------------------------------

/****************************************************************************
 * Copyright (C) 2016 CERN/Switzerland                                      *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 *     http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ***************************************************************************/

#ifndef __GRID_HAMMER_ARG_UTILS_H__
#define __GRID_HAMMER_ARG_UTILS_H__

#include <cstdio>
#include "Utils.hh"

std::string extract_opt(const option::Option &opt) {
  if(opt && opt.arg) return std::string(opt.arg);
  return "";
}

struct Arg: public option::Arg {
  static void printError(const char* msg1, const option::Option& opt, const char* msg2) {
    fprintf(stderr, "%s", msg1);
    fwrite(opt.name, opt.namelen, 1, stderr);
    fprintf(stderr, "%s", msg2);
  }

  static option::ArgStatus Unknown(const option::Option& option, bool msg) {
    if (msg) printError("Unknown option '", option, "'\n");
    return option::ARG_ILLEGAL;
  }

  static option::ArgStatus Required(const option::Option& option, bool msg) {
    if (option.arg != 0)
      return option::ARG_OK;

    if (msg) printError("Option '", option, "' requires an argument\n");
    return option::ARG_ILLEGAL;
  }

  static option::ArgStatus NonEmpty(const option::Option& option, bool msg) {
    if (option.arg != 0 && option.arg[0] != 0)
      return option::ARG_OK;

    if (msg) printError("Option '", option, "' requires a non-empty argument\n");
    return option::ARG_ILLEGAL;
  }

  static option::ArgStatus Numeric(const option::Option& option, bool msg) {
    char* endptr = 0;
    if (option.arg != 0 && strtol(option.arg, &endptr, 10)){};
    if (endptr != option.arg && *endptr == 0)
      return option::ARG_OK;

    if (msg) printError("Option '", option, "' requires a numeric argument\n");
    return option::ARG_ILLEGAL;
  }

  static option::ArgStatus Operation(const option::Option& option, bool msg) {
    if(option.arg != 0 && stringToOperation(std::string(option.arg)) != Operation::INVALID) {
      return option::ARG_OK;
    }
    if (msg) printError("Option '", option, "' is invalid.\n");
    return option::ARG_ILLEGAL;
  }

  static option::ArgStatus Boolean(const option::Option& option, bool msg) {
    bool tmp;
    if(option.arg != 0 && parseBooleanFlag(std::string(option.arg), tmp)) {
      return option::ARG_OK;
    }
    if (msg) printError("Option '", option, "' is invalid.\n");
    return option::ARG_ILLEGAL;
  }
};

#endif
