# grid-hammer

[![build status](https://gitlab.cern.ch/lcgdm/grid-hammer/badges/master/build.svg)](https://gitlab.cern.ch/lcgdm/grid-hammer/commits/master)

The grid-hammer is a load testing framework designed for use against CERN storage systems. (DPM, EOS)

Not safe to use against production instances!

Supported protocols:
* http, https
* WebDAV (only makes a difference for stat operations, compared to http)
* xroot

# How it works

There are three components:
* The low-level C++ binaries, hammer-xroot and hammer-http, which generate the actual traffic and output the results in JSON.
* The runner script, which wraps the C++ binaries, interprets the JSON and pretty-prints some statistics in the terminal, while also having the ability to store all results for later use and visualization.
* The visualizer script, which can produce graphs out of the runner script results.

# Examples

You can use the hammer binaries on their own - this is not particularly recommended or user-friendly,
as you'll just get a ton of JSON at the end.

```
hammer-xroot --url dpmhead-rc.cern.ch//dpm/cern.ch/home/dteam/some-folder/ --nthreads 1 --operation read --firstfile 0 --lastfile 100
```

It's easier to use the runner script - it runs the required commands in the background, consumes the JSON and pretty-prints some
statistics.

```
hammer-runner.py --data-repo results --url dpmhead-rc.cern.ch//dpm/cern.ch/home/dteam/hammer/ --protocols davs xroot --threads 1 2 10 100 --operations write stat read delete --runs 3 --nfiles 10000
```

# Available operations

The following operations have been implemented:
* **write**: Write small files with pre-determined contents, which contain the file ID. Each file is slightly different than the other.
* **read**: Read small files, and verify the contents.
* **stat**: Stat small files, and verify the size.
* **delete**: Delete the files.

# Packages

We use a dedicated repository for grid-hammer, add the following to `/etc/yum.repos.d`:

```
[grid-hammer-cbuild]
name=grid-hammer continuous builds from master branch
baseurl=http://storage-ci.web.cern.ch/storage-ci/grid-hammer/master/el7/$basearch
enabled=1
gpgcheck=0
protect=1
priority=20
```

Change `el7` to `el6` for SLC6.
