// ----------------------------------------------------------------------
// File: Counter.hh
// Author: Georgios Bitzes - CERN
// ----------------------------------------------------------------------

/****************************************************************************
 * Copyright (C) 2016 CERN/Switzerland                                      *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 *     http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ***************************************************************************/

#ifndef __GRID_HAMMER_COUNTER_H__
#define __GRID_HAMMER_COUNTER_H__

class Counter {
public:
  Counter(int64_t initial = 0) {
    value = initial;
  }

  int64_t getAndDecrement() {
    return __sync_fetch_and_add(&value, -1);
  }

  int64_t getAndIncrement() {
    return __sync_fetch_and_add(&value, 1);
  }

  int64_t get() {
    return __sync_fetch_and_or(&value, 0);
  }

  void reset(int64_t newval) {
    // atomic set.. hopefully I got it right :>
    int64_t oldval = value;
    while(!__sync_bool_compare_and_swap(&value, oldval, newval)) {
      oldval = value;
    }
  }

private:
  int64_t value;
};



#endif
