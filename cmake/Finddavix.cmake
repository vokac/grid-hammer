# Try to find davix
# Once done, this will define
#
# DAVIX_FOUND                - system has davix
# DAVIX_INCLUDE_DIRS         - davix include directories
# DAVIX_LIBRARY              - davix library
#
# DAVIX_ROOT_DIR may be defined as a hint for where to look

include(FindPackageHandleStandardArgs)

if(DAVIX_INCLUDE_DIRS AND DAVIX_SHARED_LIBRARY)
  set(DAVIX_FIND_QUIETLY TRUE)
else()
  find_path(
    DAVIX_INCLUDE_DIRS
    NAMES davix.hpp
    HINTS  ${DAVIX_ROOT_DIR}
    PATH_SUFFIXES include/davix)

  find_library(
    DAVIX_LIBRARY
    NAMES libdavix.so
    HINTS ${DAVIX_ROOT_DIR}
    PATH_SUFFIXES lib lib64
  )

  find_package_handle_standard_args(
    davix
    DEFAULT_MSG
    DAVIX_LIBRARY
    DAVIX_INCLUDE_DIRS)
endif()
